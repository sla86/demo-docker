package home.hello.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;


@RestController
@RequestMapping("/hello")
public class HelloController {

    Logger logger = LoggerFactory.getLogger(HelloController.class);

    @GetMapping("/{name}")
    public String hello(@PathVariable String name) {
        String message = String.format("Hello %s", name);
        //message = String.format("%s %s", message, LocalDateTime.now());
        logger.info(message);
        return message;
    }
}
