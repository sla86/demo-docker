==== 3.1	package app with Docker file

Run:	cd D:\projects\demo-docker\docker-app\hello-app
Run:	..\mvnw clean install
Check:	Dockerfile
Run:	docker build -t demo-app .
Run: 	docker run -d -p 8080:80 demo-app
Open:	http://localhost:8080/hello/slava
Run:	docker rm -f container_name

==== 3.2 package app with maven plugin

Run:	cd ..\docker
Check:	pom.xml
Run:	..\mvnw package
Run:	docker images

==== 3.3 Deliver application manually

Run:	docker push eu.gcr.io/quick-line-259809/hello_app:1.0-SNAPSHOT
Open:	https://console.cloud.google.com
Check:	Cloud run
Create: service
Open:	https://helloapp-sgf3dvceqa-ew.a.run.app/hello/slava

==== 3.4 Deliver application programatically

Check:	pom.xml
Fix:	uncomment all commented code in java code
Run:	..\mvnw deploy -Dstory=-1