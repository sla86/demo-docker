==== 0.0: Resources

https://www.docker.com/

https://docs.docker.com/

https://hub.docker.com/

==== 1.1: Oracle

TODO

==== 1.2: Kafka

Open: 	https://kafka.apache.org/quickstart

Check:	Step 1: Download the code
		Step 2: Start the server

Open: 	https://hub.docker.com/r/johnnypark/kafka-zookeeper

Copy: 	run command
Add:  	container name "kafka", add detached mode
Run: 	run command

==== 1.3: Kafka manager

Open: 	https://github.com/yahoo/kafka-manager

Check:	Requirements
		Deployment
		Starting the service

Open: 	https://hub.docker.com/r/sheepkiller/kafka-manager

Copy: 	Howto command
Add:	link to kafka container and fix ZK_HOSTS = kafka:2181
Run:	command
Open:	http://localhost:9000/	
Create:new cluster (name: kafka; host:kafka)
Create:new topic
Run:	docker rmi sheepkiller/kafka-manager