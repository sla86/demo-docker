CREATE TABLE CLIENT
(
	ID varchar(3) NOT NULL,
	NAME varchar(200) NULL,
	CRE_DATE timestamp without time zone NOT NULL,
	CRE_USER varchar(50) NOT NULL
);

ALTER TABLE CLIENT ADD CONSTRAINT PK_CLIENT
	PRIMARY KEY (ID)
;