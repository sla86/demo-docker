==== 2.1:	Run DB from default image 

Run:	docker pull postgres
Run:	docker run -e POSTGRES_PASSWORD=postgres -it postgres
Test:	connection with dbeaver
Test:	port 5432 with telnet
Run:	kill container
Run:	docker rm {container name}
Run:	docker run -e POSTGRES_PASSWORD=postgres -it -p 5432:5432 postgres
Test:	connection with dbeaver

==== 2.2:	Use docker images to create custome db image 

Run:	cd D:\projects\demo-docker\docker-db\image
Check:	Dockerfile, init_demo_db.sql
Run:	docker build -t demo-pg .
Run:	docker run -d -p 5432:5432 --name demo-pg-container demo-pg

==== 2.3:	Use docker as task runner. Flyway

TODO:// add some text

Check:	https://flywaydb.org/getstarted/firststeps/commandline
Check:	https://hub.docker.com/r/boxfuse/flyway
Check:	migration folder and file
Run:	docker run --rm -v D:\projects\demo-docker\docker-db\flyway:/flyway/sql --link demo-pg-container:demo-pg-container boxfuse/flyway migrate -url=jdbc:postgresql://demo-pg-container:5432/demo_db -user=demo_user -password=demo_user

Check:	console warning

Run:	docker run --rm -v D:\projects\demo-docker\docker-db\flyway:/flyway/sql --link demo-pg-container:demo-pg-container boxfuse/flyway clean -url=jdbc:postgresql://demo-pg-container:5432/demo_db -user=demo_user -password=demo_user

==== 2.4:	Fix errors

Run: 	docker rm -f demo-pg-container
Fix:	FROM postgres:10.8 in Dockerfile
Run:	docker build -t demo-pg .
Run:	docker run -d -p 5432:5432 --name demo-pg-container demo-pg
Run:	docker run --rm -v D:\projects\demo-docker\docker-db\flyway:/flyway/sql --link demo-pg-container:demo-pg-container boxfuse/flyway migrate -url=jdbc:postgresql://demo-pg-container:5432/demo_db -user=demo_user -password=demo_user