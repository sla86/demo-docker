CREATE DATABASE demo_db;
CREATE USER demo_user WITH ENCRYPTED PASSWORD 'demo_user';
GRANT ALL PRIVILEGES ON DATABASE demo_db TO demo_user;